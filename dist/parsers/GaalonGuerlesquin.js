'use strict';Object.defineProperty(exports, "__esModule", { value: true });var _regenerator = require('babel-runtime/regenerator');var _regenerator2 = _interopRequireDefault(_regenerator);var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);var _createClass2 = require('babel-runtime/helpers/createClass');var _createClass3 = _interopRequireDefault(_createClass2);var _jsdom = require('jsdom');function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}var

GaalonGuerlesquin = function () {
  function GaalonGuerlesquin(url) {(0, _classCallCheck3.default)(this, GaalonGuerlesquin);
    this.url = url;
    this.baseUrl = 'https://www.gaalon-guerlesquin.fr';
  }(0, _createClass3.default)(GaalonGuerlesquin, [{ key: 'parse', value: function () {var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {var dom, adsList, ads;return _regenerator2.default.wrap(function _callee$(_context) {while (1) {switch (_context.prev = _context.next) {case 0:_context.next = 2;return (


                  _jsdom.JSDOM.fromURL(this.url, {
                    referrer: this.baseUrl }).
                  catch(function (err) {return console.log(err);}));case 2:dom = _context.sent;

                adsList = dom.window.document.querySelectorAll('.teaser');

                ads = [];

                Array.prototype.forEach.call(adsList, function (ad) {
                  var link = ad.getAttribute('data-url');
                  var id = /(\d+)\?/g.exec(link);

                  ads.push({
                    id: parseInt(id[1], 10),
                    title: 'GG - ' + ad.getAttribute('data-title').trim() + ' ' + ad.querySelector('.teaser__location').textContent.trim(),
                    image: ad.querySelector('img').getAttribute('src'),
                    link: link });

                });return _context.abrupt('return',

                ads);case 7:case 'end':return _context.stop();}}}, _callee, this);}));function parse() {return _ref.apply(this, arguments);}return parse;}() }]);return GaalonGuerlesquin;}();exports.default = GaalonGuerlesquin;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9wYXJzZXJzL0dhYWxvbkd1ZXJsZXNxdWluLmpzIl0sIm5hbWVzIjpbIkdhYWxvbkd1ZXJsZXNxdWluIiwidXJsIiwiYmFzZVVybCIsIkpTRE9NIiwiZnJvbVVSTCIsInJlZmVycmVyIiwiY2F0Y2giLCJjb25zb2xlIiwibG9nIiwiZXJyIiwiZG9tIiwiYWRzTGlzdCIsIndpbmRvdyIsImRvY3VtZW50IiwicXVlcnlTZWxlY3RvckFsbCIsImFkcyIsIkFycmF5IiwicHJvdG90eXBlIiwiZm9yRWFjaCIsImNhbGwiLCJhZCIsImxpbmsiLCJnZXRBdHRyaWJ1dGUiLCJpZCIsImV4ZWMiLCJwdXNoIiwicGFyc2VJbnQiLCJ0aXRsZSIsInRyaW0iLCJxdWVyeVNlbGVjdG9yIiwidGV4dENvbnRlbnQiLCJpbWFnZSJdLCJtYXBwaW5ncyI6IjZrQkFBQSw4Qjs7QUFFcUJBLGlCO0FBQ25CLDZCQUFZQyxHQUFaLEVBQWlCO0FBQ2YsU0FBS0EsR0FBTCxHQUFXQSxHQUFYO0FBQ0EsU0FBS0MsT0FBTCxHQUFlLG1DQUFmO0FBQ0QsRzs7O0FBR21CQywrQkFBTUMsT0FBTixDQUFjLEtBQUtILEdBQW5CLEVBQXdCO0FBQ3hDSSw4QkFBVSxLQUFLSCxPQUR5QixFQUF4QjtBQUVmSSx1QkFGZSxDQUVULHVCQUFPQyxRQUFRQyxHQUFSLENBQVlDLEdBQVosQ0FBUCxFQUZTLEMsU0FBWkMsRzs7QUFJQUMsdUIsR0FBVUQsSUFBSUUsTUFBSixDQUFXQyxRQUFYLENBQW9CQyxnQkFBcEIsQ0FBcUMsU0FBckMsQzs7QUFFVkMsbUIsR0FBTSxFOztBQUVaQyxzQkFBTUMsU0FBTixDQUFnQkMsT0FBaEIsQ0FBd0JDLElBQXhCLENBQTZCUixPQUE3QixFQUFzQyxVQUFDUyxFQUFELEVBQVE7QUFDNUMsc0JBQU1DLE9BQU9ELEdBQUdFLFlBQUgsQ0FBZ0IsVUFBaEIsQ0FBYjtBQUNBLHNCQUFNQyxLQUFLLFdBQVdDLElBQVgsQ0FBZ0JILElBQWhCLENBQVg7O0FBRUFOLHNCQUFJVSxJQUFKLENBQVM7QUFDUEYsd0JBQUlHLFNBQVNILEdBQUcsQ0FBSCxDQUFULEVBQWdCLEVBQWhCLENBREc7QUFFUEkscUNBQWVQLEdBQUdFLFlBQUgsQ0FBZ0IsWUFBaEIsRUFBOEJNLElBQTlCLEVBQWYsU0FBdURSLEdBQUdTLGFBQUgsQ0FBaUIsbUJBQWpCLEVBQXNDQyxXQUF0QyxDQUFrREYsSUFBbEQsRUFGaEQ7QUFHUEcsMkJBQU9YLEdBQUdTLGFBQUgsQ0FBaUIsS0FBakIsRUFBd0JQLFlBQXhCLENBQXFDLEtBQXJDLENBSEE7QUFJUEQsOEJBSk8sRUFBVDs7QUFNRCxpQkFWRCxFOztBQVlPTixtQiw2TEEzQlVmLGlCIiwiZmlsZSI6IkdhYWxvbkd1ZXJsZXNxdWluLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSlNET00gfSBmcm9tICdqc2RvbSc7XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEdhYWxvbkd1ZXJsZXNxdWluIHtcbiAgY29uc3RydWN0b3IodXJsKSB7XG4gICAgdGhpcy51cmwgPSB1cmw7XG4gICAgdGhpcy5iYXNlVXJsID0gJ2h0dHBzOi8vd3d3LmdhYWxvbi1ndWVybGVzcXVpbi5mcic7XG4gIH1cblxuICBhc3luYyBwYXJzZSgpIHtcbiAgICBjb25zdCBkb20gPSBhd2FpdCBKU0RPTS5mcm9tVVJMKHRoaXMudXJsLCB7XG4gICAgICByZWZlcnJlcjogdGhpcy5iYXNlVXJsLFxuICAgIH0pLmNhdGNoKGVyciA9PiBjb25zb2xlLmxvZyhlcnIpKTtcblxuICAgIGNvbnN0IGFkc0xpc3QgPSBkb20ud2luZG93LmRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy50ZWFzZXInKTtcblxuICAgIGNvbnN0IGFkcyA9IFtdO1xuXG4gICAgQXJyYXkucHJvdG90eXBlLmZvckVhY2guY2FsbChhZHNMaXN0LCAoYWQpID0+IHtcbiAgICAgIGNvbnN0IGxpbmsgPSBhZC5nZXRBdHRyaWJ1dGUoJ2RhdGEtdXJsJyk7XG4gICAgICBjb25zdCBpZCA9IC8oXFxkKylcXD8vZy5leGVjKGxpbmspO1xuXG4gICAgICBhZHMucHVzaCh7XG4gICAgICAgIGlkOiBwYXJzZUludChpZFsxXSwgMTApLFxuICAgICAgICB0aXRsZTogYEdHIC0gJHthZC5nZXRBdHRyaWJ1dGUoJ2RhdGEtdGl0bGUnKS50cmltKCl9ICR7YWQucXVlcnlTZWxlY3RvcignLnRlYXNlcl9fbG9jYXRpb24nKS50ZXh0Q29udGVudC50cmltKCl9YCxcbiAgICAgICAgaW1hZ2U6IGFkLnF1ZXJ5U2VsZWN0b3IoJ2ltZycpLmdldEF0dHJpYnV0ZSgnc3JjJyksXG4gICAgICAgIGxpbmssXG4gICAgICB9KTtcbiAgICB9KTtcblxuICAgIHJldHVybiBhZHM7XG4gIH1cbn1cbiJdfQ==
'use strict';Object.defineProperty(exports, "__esModule", { value: true });var _regenerator = require('babel-runtime/regenerator');var _regenerator2 = _interopRequireDefault(_regenerator);var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);var _createClass2 = require('babel-runtime/helpers/createClass');var _createClass3 = _interopRequireDefault(_createClass2);var _jsdom = require('jsdom');function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}var

LaMotte = function () {
  function LaMotte(url) {(0, _classCallCheck3.default)(this, LaMotte);
    this.url = url;
    this.baseUrl = 'https://www.lamotte-gestion.fr';
  }(0, _createClass3.default)(LaMotte, [{ key: 'parse', value: function () {var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {var _this = this;var dom, adsList, ads;return _regenerator2.default.wrap(function _callee$(_context) {while (1) {switch (_context.prev = _context.next) {case 0:_context.next = 2;return (


                  _jsdom.JSDOM.fromURL(this.url, {
                    referrer: this.baseUrl }).
                  catch(function (err) {return console.log(err);}));case 2:dom = _context.sent;

                adsList = dom.window.document.querySelectorAll('.programme');

                ads = [];

                Array.prototype.forEach.call(adsList, function (ad) {
                  var link = ad.querySelector('a:first-child').getAttribute('href');
                  var id = /(\d+)$/g.exec(link);

                  ads.push({
                    id: parseInt(id[1], 10),
                    title: 'LM - ' + ad.querySelector('.nom-programme').textContent.trim() + ' ' + ad.querySelector('.reference').textContent.trim(),
                    image: ad.querySelector('.programme-img img').getAttribute('src'),
                    link: _this.baseUrl + link });

                });return _context.abrupt('return',

                ads);case 7:case 'end':return _context.stop();}}}, _callee, this);}));function parse() {return _ref.apply(this, arguments);}return parse;}() }]);return LaMotte;}();exports.default = LaMotte;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9wYXJzZXJzL0xhTW90dGUuanMiXSwibmFtZXMiOlsiTGFNb3R0ZSIsInVybCIsImJhc2VVcmwiLCJKU0RPTSIsImZyb21VUkwiLCJyZWZlcnJlciIsImNhdGNoIiwiY29uc29sZSIsImxvZyIsImVyciIsImRvbSIsImFkc0xpc3QiLCJ3aW5kb3ciLCJkb2N1bWVudCIsInF1ZXJ5U2VsZWN0b3JBbGwiLCJhZHMiLCJBcnJheSIsInByb3RvdHlwZSIsImZvckVhY2giLCJjYWxsIiwiYWQiLCJsaW5rIiwicXVlcnlTZWxlY3RvciIsImdldEF0dHJpYnV0ZSIsImlkIiwiZXhlYyIsInB1c2giLCJwYXJzZUludCIsInRpdGxlIiwidGV4dENvbnRlbnQiLCJ0cmltIiwiaW1hZ2UiXSwibWFwcGluZ3MiOiI2a0JBQUEsOEI7O0FBRXFCQSxPO0FBQ25CLG1CQUFZQyxHQUFaLEVBQWlCO0FBQ2YsU0FBS0EsR0FBTCxHQUFXQSxHQUFYO0FBQ0EsU0FBS0MsT0FBTCxHQUFlLGdDQUFmO0FBQ0QsRzs7O0FBR21CQywrQkFBTUMsT0FBTixDQUFjLEtBQUtILEdBQW5CLEVBQXdCO0FBQ3hDSSw4QkFBVSxLQUFLSCxPQUR5QixFQUF4QjtBQUVmSSx1QkFGZSxDQUVULHVCQUFPQyxRQUFRQyxHQUFSLENBQVlDLEdBQVosQ0FBUCxFQUZTLEMsU0FBWkMsRzs7QUFJQUMsdUIsR0FBVUQsSUFBSUUsTUFBSixDQUFXQyxRQUFYLENBQW9CQyxnQkFBcEIsQ0FBcUMsWUFBckMsQzs7QUFFVkMsbUIsR0FBTSxFOztBQUVaQyxzQkFBTUMsU0FBTixDQUFnQkMsT0FBaEIsQ0FBd0JDLElBQXhCLENBQTZCUixPQUE3QixFQUFzQyxVQUFDUyxFQUFELEVBQVE7QUFDNUMsc0JBQU1DLE9BQU9ELEdBQUdFLGFBQUgsQ0FBaUIsZUFBakIsRUFBa0NDLFlBQWxDLENBQStDLE1BQS9DLENBQWI7QUFDQSxzQkFBTUMsS0FBSyxVQUFVQyxJQUFWLENBQWVKLElBQWYsQ0FBWDs7QUFFQU4sc0JBQUlXLElBQUosQ0FBUztBQUNQRix3QkFBSUcsU0FBU0gsR0FBRyxDQUFILENBQVQsRUFBZ0IsRUFBaEIsQ0FERztBQUVQSSxxQ0FBZVIsR0FBR0UsYUFBSCxDQUFpQixnQkFBakIsRUFBbUNPLFdBQW5DLENBQStDQyxJQUEvQyxFQUFmLFNBQXdFVixHQUFHRSxhQUFILENBQWlCLFlBQWpCLEVBQStCTyxXQUEvQixDQUEyQ0MsSUFBM0MsRUFGakU7QUFHUEMsMkJBQU9YLEdBQUdFLGFBQUgsQ0FBaUIsb0JBQWpCLEVBQXVDQyxZQUF2QyxDQUFvRCxLQUFwRCxDQUhBO0FBSVBGLDBCQUFNLE1BQUtuQixPQUFMLEdBQWVtQixJQUpkLEVBQVQ7O0FBTUQsaUJBVkQsRTs7QUFZT04sbUIsbUxBM0JVZixPIiwiZmlsZSI6IkxhTW90dGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBKU0RPTSB9IGZyb20gJ2pzZG9tJztcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgTGFNb3R0ZSB7XG4gIGNvbnN0cnVjdG9yKHVybCkge1xuICAgIHRoaXMudXJsID0gdXJsO1xuICAgIHRoaXMuYmFzZVVybCA9ICdodHRwczovL3d3dy5sYW1vdHRlLWdlc3Rpb24uZnInO1xuICB9XG5cbiAgYXN5bmMgcGFyc2UoKSB7XG4gICAgY29uc3QgZG9tID0gYXdhaXQgSlNET00uZnJvbVVSTCh0aGlzLnVybCwge1xuICAgICAgcmVmZXJyZXI6IHRoaXMuYmFzZVVybCxcbiAgICB9KS5jYXRjaChlcnIgPT4gY29uc29sZS5sb2coZXJyKSk7XG5cbiAgICBjb25zdCBhZHNMaXN0ID0gZG9tLndpbmRvdy5kb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcucHJvZ3JhbW1lJyk7XG5cbiAgICBjb25zdCBhZHMgPSBbXTtcblxuICAgIEFycmF5LnByb3RvdHlwZS5mb3JFYWNoLmNhbGwoYWRzTGlzdCwgKGFkKSA9PiB7XG4gICAgICBjb25zdCBsaW5rID0gYWQucXVlcnlTZWxlY3RvcignYTpmaXJzdC1jaGlsZCcpLmdldEF0dHJpYnV0ZSgnaHJlZicpO1xuICAgICAgY29uc3QgaWQgPSAvKFxcZCspJC9nLmV4ZWMobGluayk7XG5cbiAgICAgIGFkcy5wdXNoKHtcbiAgICAgICAgaWQ6IHBhcnNlSW50KGlkWzFdLCAxMCksXG4gICAgICAgIHRpdGxlOiBgTE0gLSAke2FkLnF1ZXJ5U2VsZWN0b3IoJy5ub20tcHJvZ3JhbW1lJykudGV4dENvbnRlbnQudHJpbSgpfSAke2FkLnF1ZXJ5U2VsZWN0b3IoJy5yZWZlcmVuY2UnKS50ZXh0Q29udGVudC50cmltKCl9YCxcbiAgICAgICAgaW1hZ2U6IGFkLnF1ZXJ5U2VsZWN0b3IoJy5wcm9ncmFtbWUtaW1nIGltZycpLmdldEF0dHJpYnV0ZSgnc3JjJyksXG4gICAgICAgIGxpbms6IHRoaXMuYmFzZVVybCArIGxpbmssXG4gICAgICB9KTtcbiAgICB9KTtcblxuICAgIHJldHVybiBhZHM7XG4gIH1cbn1cbiJdfQ==
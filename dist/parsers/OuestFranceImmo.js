'use strict';Object.defineProperty(exports, "__esModule", { value: true });var _regenerator = require('babel-runtime/regenerator');var _regenerator2 = _interopRequireDefault(_regenerator);var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);var _createClass2 = require('babel-runtime/helpers/createClass');var _createClass3 = _interopRequireDefault(_createClass2);var _jsdom = require('jsdom');function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}var

OuestFranceImmo = function () {
  function OuestFranceImmo(url) {(0, _classCallCheck3.default)(this, OuestFranceImmo);
    this.url = url;
    this.baseUrl = 'https://www.ouestfrance-immo.com';
  }(0, _createClass3.default)(OuestFranceImmo, [{ key: 'parse', value: function () {var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {var dom, adsList, ads, baseUrl;return _regenerator2.default.wrap(function _callee$(_context) {while (1) {switch (_context.prev = _context.next) {case 0:_context.next = 2;return (


                  _jsdom.JSDOM.fromURL(this.url, {
                    referrer: this.baseUrl }).
                  catch(function (err) {return console.log(err);}));case 2:dom = _context.sent;

                adsList = dom.window.document.querySelectorAll('.annLink');

                ads = [];

                baseUrl = this.baseUrl;

                Array.prototype.forEach.call(adsList, function (ad) {
                  if (!ad.querySelector('div:first-child').classList.contains('premium')) {
                    // const adDate = ad.querySelector('.annDebAff')
                    //   ? new Date(ad.querySelector('.annDebAff').textContent.trim())
                    //   : new Date();
                    var adAdresse = ad.querySelector('.annAdresse') ? ad.querySelector('.annAdresse').textContent.trim() : '';

                    ads.push({
                      id: parseInt(ad.querySelector('div:first-child').getAttribute('data-id'), 10),
                      title: 'OFI - ' + ad.querySelector('.annTitre').textContent.trim() + ' ' + adAdresse,
                      // date: `${adDate.getHours()}:${adDate.getMinutes()}`,
                      // location: adAdresse,
                      // price: ad.querySelector('.annPrix').textContent.trim(),
                      // category: null,
                      image: ad.querySelector('.annPhoto').getAttribute('data-original'),
                      link: baseUrl + ad.getAttribute('href') });

                  }
                });return _context.abrupt('return',

                ads);case 8:case 'end':return _context.stop();}}}, _callee, this);}));function parse() {return _ref.apply(this, arguments);}return parse;}() }]);return OuestFranceImmo;}();exports.default = OuestFranceImmo;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9wYXJzZXJzL091ZXN0RnJhbmNlSW1tby5qcyJdLCJuYW1lcyI6WyJPdWVzdEZyYW5jZUltbW8iLCJ1cmwiLCJiYXNlVXJsIiwiSlNET00iLCJmcm9tVVJMIiwicmVmZXJyZXIiLCJjYXRjaCIsImNvbnNvbGUiLCJsb2ciLCJlcnIiLCJkb20iLCJhZHNMaXN0Iiwid2luZG93IiwiZG9jdW1lbnQiLCJxdWVyeVNlbGVjdG9yQWxsIiwiYWRzIiwiQXJyYXkiLCJwcm90b3R5cGUiLCJmb3JFYWNoIiwiY2FsbCIsImFkIiwicXVlcnlTZWxlY3RvciIsImNsYXNzTGlzdCIsImNvbnRhaW5zIiwiYWRBZHJlc3NlIiwidGV4dENvbnRlbnQiLCJ0cmltIiwicHVzaCIsImlkIiwicGFyc2VJbnQiLCJnZXRBdHRyaWJ1dGUiLCJ0aXRsZSIsImltYWdlIiwibGluayJdLCJtYXBwaW5ncyI6IjZrQkFBQSw4Qjs7QUFFcUJBLGU7QUFDbkIsMkJBQVlDLEdBQVosRUFBaUI7QUFDZixTQUFLQSxHQUFMLEdBQVdBLEdBQVg7QUFDQSxTQUFLQyxPQUFMLEdBQWUsa0NBQWY7QUFDRCxHOzs7QUFHbUJDLCtCQUFNQyxPQUFOLENBQWMsS0FBS0gsR0FBbkIsRUFBd0I7QUFDeENJLDhCQUFVLEtBQUtILE9BRHlCLEVBQXhCO0FBRWZJLHVCQUZlLENBRVQsdUJBQU9DLFFBQVFDLEdBQVIsQ0FBWUMsR0FBWixDQUFQLEVBRlMsQyxTQUFaQyxHOztBQUlBQyx1QixHQUFVRCxJQUFJRSxNQUFKLENBQVdDLFFBQVgsQ0FBb0JDLGdCQUFwQixDQUFxQyxVQUFyQyxDOztBQUVWQyxtQixHQUFNLEU7O0FBRUpiLHVCLEdBQVksSSxDQUFaQSxPOztBQUVSYyxzQkFBTUMsU0FBTixDQUFnQkMsT0FBaEIsQ0FBd0JDLElBQXhCLENBQTZCUixPQUE3QixFQUFzQyxVQUFDUyxFQUFELEVBQVE7QUFDNUMsc0JBQUksQ0FBQ0EsR0FBR0MsYUFBSCxDQUFpQixpQkFBakIsRUFBb0NDLFNBQXBDLENBQThDQyxRQUE5QyxDQUF1RCxTQUF2RCxDQUFMLEVBQXdFO0FBQ3RFO0FBQ0E7QUFDQTtBQUNBLHdCQUFNQyxZQUFZSixHQUFHQyxhQUFILENBQWlCLGFBQWpCLElBQWtDRCxHQUFHQyxhQUFILENBQWlCLGFBQWpCLEVBQWdDSSxXQUFoQyxDQUE0Q0MsSUFBNUMsRUFBbEMsR0FBdUYsRUFBekc7O0FBRUFYLHdCQUFJWSxJQUFKLENBQVM7QUFDUEMsMEJBQUlDLFNBQVNULEdBQUdDLGFBQUgsQ0FBaUIsaUJBQWpCLEVBQW9DUyxZQUFwQyxDQUFpRCxTQUFqRCxDQUFULEVBQXNFLEVBQXRFLENBREc7QUFFUEMsd0NBQWdCWCxHQUFHQyxhQUFILENBQWlCLFdBQWpCLEVBQThCSSxXQUE5QixDQUEwQ0MsSUFBMUMsRUFBaEIsU0FBb0VGLFNBRjdEO0FBR1A7QUFDQTtBQUNBO0FBQ0E7QUFDQVEsNkJBQU9aLEdBQUdDLGFBQUgsQ0FBaUIsV0FBakIsRUFBOEJTLFlBQTlCLENBQTJDLGVBQTNDLENBUEE7QUFRUEcsNEJBQU0vQixVQUFVa0IsR0FBR1UsWUFBSCxDQUFnQixNQUFoQixDQVJULEVBQVQ7O0FBVUQ7QUFDRixpQkFsQkQsRTs7QUFvQk9mLG1CLDJMQXJDVWYsZSIsImZpbGUiOiJPdWVzdEZyYW5jZUltbW8uanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBKU0RPTSB9IGZyb20gJ2pzZG9tJztcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgT3Vlc3RGcmFuY2VJbW1vIHtcbiAgY29uc3RydWN0b3IodXJsKSB7XG4gICAgdGhpcy51cmwgPSB1cmw7XG4gICAgdGhpcy5iYXNlVXJsID0gJ2h0dHBzOi8vd3d3Lm91ZXN0ZnJhbmNlLWltbW8uY29tJztcbiAgfVxuXG4gIGFzeW5jIHBhcnNlKCkge1xuICAgIGNvbnN0IGRvbSA9IGF3YWl0IEpTRE9NLmZyb21VUkwodGhpcy51cmwsIHtcbiAgICAgIHJlZmVycmVyOiB0aGlzLmJhc2VVcmwsXG4gICAgfSkuY2F0Y2goZXJyID0+IGNvbnNvbGUubG9nKGVycikpO1xuXG4gICAgY29uc3QgYWRzTGlzdCA9IGRvbS53aW5kb3cuZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLmFubkxpbmsnKTtcblxuICAgIGNvbnN0IGFkcyA9IFtdO1xuXG4gICAgY29uc3QgeyBiYXNlVXJsIH0gPSB0aGlzO1xuXG4gICAgQXJyYXkucHJvdG90eXBlLmZvckVhY2guY2FsbChhZHNMaXN0LCAoYWQpID0+IHtcbiAgICAgIGlmICghYWQucXVlcnlTZWxlY3RvcignZGl2OmZpcnN0LWNoaWxkJykuY2xhc3NMaXN0LmNvbnRhaW5zKCdwcmVtaXVtJykpIHtcbiAgICAgICAgLy8gY29uc3QgYWREYXRlID0gYWQucXVlcnlTZWxlY3RvcignLmFubkRlYkFmZicpXG4gICAgICAgIC8vICAgPyBuZXcgRGF0ZShhZC5xdWVyeVNlbGVjdG9yKCcuYW5uRGViQWZmJykudGV4dENvbnRlbnQudHJpbSgpKVxuICAgICAgICAvLyAgIDogbmV3IERhdGUoKTtcbiAgICAgICAgY29uc3QgYWRBZHJlc3NlID0gYWQucXVlcnlTZWxlY3RvcignLmFubkFkcmVzc2UnKSA/IGFkLnF1ZXJ5U2VsZWN0b3IoJy5hbm5BZHJlc3NlJykudGV4dENvbnRlbnQudHJpbSgpIDogJyc7XG5cbiAgICAgICAgYWRzLnB1c2goe1xuICAgICAgICAgIGlkOiBwYXJzZUludChhZC5xdWVyeVNlbGVjdG9yKCdkaXY6Zmlyc3QtY2hpbGQnKS5nZXRBdHRyaWJ1dGUoJ2RhdGEtaWQnKSwgMTApLFxuICAgICAgICAgIHRpdGxlOiBgT0ZJIC0gJHthZC5xdWVyeVNlbGVjdG9yKCcuYW5uVGl0cmUnKS50ZXh0Q29udGVudC50cmltKCl9ICR7YWRBZHJlc3NlfWAsXG4gICAgICAgICAgLy8gZGF0ZTogYCR7YWREYXRlLmdldEhvdXJzKCl9OiR7YWREYXRlLmdldE1pbnV0ZXMoKX1gLFxuICAgICAgICAgIC8vIGxvY2F0aW9uOiBhZEFkcmVzc2UsXG4gICAgICAgICAgLy8gcHJpY2U6IGFkLnF1ZXJ5U2VsZWN0b3IoJy5hbm5Qcml4JykudGV4dENvbnRlbnQudHJpbSgpLFxuICAgICAgICAgIC8vIGNhdGVnb3J5OiBudWxsLFxuICAgICAgICAgIGltYWdlOiBhZC5xdWVyeVNlbGVjdG9yKCcuYW5uUGhvdG8nKS5nZXRBdHRyaWJ1dGUoJ2RhdGEtb3JpZ2luYWwnKSxcbiAgICAgICAgICBsaW5rOiBiYXNlVXJsICsgYWQuZ2V0QXR0cmlidXRlKCdocmVmJyksXG4gICAgICAgIH0pO1xuICAgICAgfVxuICAgIH0pO1xuXG4gICAgcmV0dXJuIGFkcztcbiAgfVxufVxuIl19
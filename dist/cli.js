#!/usr/bin/env node
'use strict';var _regenerator = require('babel-runtime/regenerator');var _regenerator2 = _interopRequireDefault(_regenerator);var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);var _toArray2 = require('babel-runtime/helpers/toArray');var _toArray3 = _interopRequireDefault(_toArray2);var init = function () {var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(















  function _callee() {var settings, ads, parser, _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, parserSettings, parserClass, parsedAds, notifier, _iteratorNormalCompletion2, _didIteratorError2, _iteratorError2, _iterator2, _step2, ad;return _regenerator2.default.wrap(function _callee$(_context) {while (1) {switch (_context.prev = _context.next) {case 0:
            settings = require('../config/searches/' + settingsFileName + '.js'); // eslint-disable-line global-require,import/no-dynamic-require
            settings = settings.default;

            ads = [];
            parser = new _Parser2.default();_iteratorNormalCompletion = true;_didIteratorError = false;_iteratorError = undefined;_context.prev = 7;_iterator =

            settings.parsers[Symbol.iterator]();case 9:if (_iteratorNormalCompletion = (_step = _iterator.next()).done) {_context.next = 22;break;}parserSettings = _step.value;
            /* eslint-disable no-await-in-loop */
            parserClass = require('./parsers/' + parserSettings.parser + '.js'); // eslint-disable-line global-require,import/no-dynamic-require

            parser.setUrl(parserSettings.url);
            parser.setSearchId(settingsFileName);
            parser.setParser(parserClass.default);_context.next = 17;return (
              parser.parse().catch(function (err) {return console.log(err);}));case 17:parsedAds = _context.sent;
            ads = ads.concat(parsedAds);case 19:_iteratorNormalCompletion = true;_context.next = 9;break;case 22:_context.next = 28;break;case 24:_context.prev = 24;_context.t0 = _context['catch'](7);_didIteratorError = true;_iteratorError = _context.t0;case 28:_context.prev = 28;_context.prev = 29;if (!_iteratorNormalCompletion && _iterator.return) {_iterator.return();}case 31:_context.prev = 31;if (!_didIteratorError) {_context.next = 34;break;}throw _iteratorError;case 34:return _context.finish(31);case 35:return _context.finish(28);case 36:


            notifier = new _Notifier2.default(settings.ifttt.event, settings.ifttt.key);

            /* eslint 'no-restricted-syntax': ['error', 'LabeledStatement', 'WithStatement'] */_iteratorNormalCompletion2 = true;_didIteratorError2 = false;_iteratorError2 = undefined;_context.prev = 40;_iterator2 =
            ads[Symbol.iterator]();case 42:if (_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done) {_context.next = 49;break;}ad = _step2.value;_context.next = 46;return (

              notifier.notify(ad.title, ad.link, ad.image).catch(function (err) {return console.log(err);}));case 46:_iteratorNormalCompletion2 = true;_context.next = 42;break;case 49:_context.next = 55;break;case 51:_context.prev = 51;_context.t1 = _context['catch'](40);_didIteratorError2 = true;_iteratorError2 = _context.t1;case 55:_context.prev = 55;_context.prev = 56;if (!_iteratorNormalCompletion2 && _iterator2.return) {_iterator2.return();}case 58:_context.prev = 58;if (!_didIteratorError2) {_context.next = 61;break;}throw _iteratorError2;case 61:return _context.finish(58);case 62:return _context.finish(55);case 63:case 'end':return _context.stop();}}}, _callee, this, [[7, 24, 28, 36], [29,, 31, 35], [40, 51, 55, 63], [56,, 58, 62]]);}));return function init() {return _ref.apply(this, arguments);};}();require('babel-polyfill');require('babel-core/register');var _nodeCron = require('node-cron');var _nodeCron2 = _interopRequireDefault(_nodeCron);var _Parser = require('./Parser');var _Parser2 = _interopRequireDefault(_Parser);var _Notifier = require('./Notifier');var _Notifier2 = _interopRequireDefault(_Notifier);function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}var _process$argv = (0, _toArray3.default)(process.argv),args = _process$argv.slice(2);var settingsFileName = args[0];if (settingsFileName === undefined) {throw Error('Config name not provided !\nUsage : lbc-parser [configName]\n  configName: searches config file name\n\n');}



_nodeCron2.default.schedule('* * * * *', (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2() {return _regenerator2.default.wrap(function _callee2$(_context2) {while (1) {switch (_context2.prev = _context2.next) {case 0:
          console.info('[' + settingsFileName + '] Parsing started ...');_context2.next = 3;return (
            init());case 3:
          console.info('[' + settingsFileName + '] Parsing ended !');case 4:case 'end':return _context2.stop();}}}, _callee2, undefined);})));
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9jbGkuanMiXSwibmFtZXMiOlsic2V0dGluZ3MiLCJyZXF1aXJlIiwic2V0dGluZ3NGaWxlTmFtZSIsImRlZmF1bHQiLCJhZHMiLCJwYXJzZXIiLCJQYXJzZXIiLCJwYXJzZXJzIiwicGFyc2VyU2V0dGluZ3MiLCJwYXJzZXJDbGFzcyIsInNldFVybCIsInVybCIsInNldFNlYXJjaElkIiwic2V0UGFyc2VyIiwicGFyc2UiLCJjYXRjaCIsImNvbnNvbGUiLCJsb2ciLCJlcnIiLCJwYXJzZWRBZHMiLCJjb25jYXQiLCJub3RpZmllciIsIk5vdGlmaWVyIiwiaWZ0dHQiLCJldmVudCIsImtleSIsImFkIiwibm90aWZ5IiwidGl0bGUiLCJsaW5rIiwiaW1hZ2UiLCJpbml0IiwicHJvY2VzcyIsImFyZ3YiLCJhcmdzIiwidW5kZWZpbmVkIiwiRXJyb3IiLCJDcm9uIiwic2NoZWR1bGUiLCJpbmZvIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBZ0JBO0FBQ01BLG9CQUROLEdBQ2lCQyxnQ0FBOEJDLGdCQUE5QixTQURqQixFQUN1RTtBQUNyRUYsdUJBQVdBLFNBQVNHLE9BQXBCOztBQUVJQyxlQUpOLEdBSVksRUFKWjtBQUtRQyxrQkFMUixHQUtpQixJQUFJQyxnQkFBSixFQUxqQjs7QUFPK0JOLHFCQUFTTyxPQVB4Qyx1SEFPYUMsY0FQYjtBQVFJO0FBQ01DLHVCQVRWLEdBU3dCUix1QkFBcUJPLGVBQWVILE1BQXBDLFNBVHhCLEVBUzBFOztBQUV0RUEsbUJBQU9LLE1BQVAsQ0FBY0YsZUFBZUcsR0FBN0I7QUFDQU4sbUJBQU9PLFdBQVAsQ0FBbUJWLGdCQUFuQjtBQUNBRyxtQkFBT1EsU0FBUCxDQUFpQkosWUFBWU4sT0FBN0IsRUFiSjtBQWM0QkUscUJBQU9TLEtBQVAsR0FBZUMsS0FBZixDQUFxQix1QkFBT0MsUUFBUUMsR0FBUixDQUFZQyxHQUFaLENBQVAsRUFBckIsQ0FkNUIsVUFjVUMsU0FkVjtBQWVJZixrQkFBTUEsSUFBSWdCLE1BQUosQ0FBV0QsU0FBWCxDQUFOLENBZko7OztBQWtCUUUsb0JBbEJSLEdBa0JtQixJQUFJQyxrQkFBSixDQUFhdEIsU0FBU3VCLEtBQVQsQ0FBZUMsS0FBNUIsRUFBbUN4QixTQUFTdUIsS0FBVCxDQUFlRSxHQUFsRCxDQWxCbkI7O0FBb0JFLCtGQXBCRjtBQXFCbUJyQixlQXJCbkIsMkhBcUJhc0IsRUFyQmI7O0FBdUJVTCx1QkFBU00sTUFBVCxDQUFnQkQsR0FBR0UsS0FBbkIsRUFBMEJGLEdBQUdHLElBQTdCLEVBQW1DSCxHQUFHSSxLQUF0QyxFQUE2Q2YsS0FBN0MsQ0FBbUQsdUJBQU9DLFFBQVFDLEdBQVIsQ0FBWUMsR0FBWixDQUFQLEVBQW5ELENBdkJWLG9vQixtQkFBZWEsSSw2Q0FkZiwwQkFDQSwrQkFDQSxxQyxtREFDQSxrQywrQ0FDQSxzQywyTEFFcUJDLFFBQVFDLEksRUFBaEJDLEksMEJBRWIsSUFBTWhDLG1CQUFtQmdDLEtBQUssQ0FBTCxDQUF6QixDQUVBLElBQUloQyxxQkFBcUJpQyxTQUF6QixFQUFvQyxDQUNsQyxNQUFNQyxNQUFNLDBHQUFOLENBQU4sQ0FDRDs7OztBQTZCREMsbUJBQUtDLFFBQUwsQ0FBYyxXQUFkLDJFQUEyQjtBQUN6QnRCLGtCQUFRdUIsSUFBUixPQUFpQnJDLGdCQUFqQiw0QkFEeUI7QUFFbkI2QixrQkFGbUI7QUFHekJmLGtCQUFRdUIsSUFBUixPQUFpQnJDLGdCQUFqQix3QkFIeUIscUVBQTNCIiwiZmlsZSI6ImNsaS5qcyIsInNvdXJjZXNDb250ZW50IjpbIlxuXG5pbXBvcnQgJ2JhYmVsLXBvbHlmaWxsJztcbmltcG9ydCAnYmFiZWwtY29yZS9yZWdpc3Rlcic7XG5pbXBvcnQgQ3JvbiBmcm9tICdub2RlLWNyb24nO1xuaW1wb3J0IFBhcnNlciBmcm9tICcuL1BhcnNlcic7XG5pbXBvcnQgTm90aWZpZXIgZnJvbSAnLi9Ob3RpZmllcic7XG5cbmNvbnN0IFssLCAuLi5hcmdzXSA9IHByb2Nlc3MuYXJndjtcblxuY29uc3Qgc2V0dGluZ3NGaWxlTmFtZSA9IGFyZ3NbMF07XG5cbmlmIChzZXR0aW5nc0ZpbGVOYW1lID09PSB1bmRlZmluZWQpIHtcbiAgdGhyb3cgRXJyb3IoJ0NvbmZpZyBuYW1lIG5vdCBwcm92aWRlZCAhXFxuVXNhZ2UgOiBsYmMtcGFyc2VyIFtjb25maWdOYW1lXVxcbiAgY29uZmlnTmFtZTogc2VhcmNoZXMgY29uZmlnIGZpbGUgbmFtZVxcblxcbicpO1xufVxuXG5hc3luYyBmdW5jdGlvbiBpbml0KCkge1xuICBsZXQgc2V0dGluZ3MgPSByZXF1aXJlKGAuLi9jb25maWcvc2VhcmNoZXMvJHtzZXR0aW5nc0ZpbGVOYW1lfS5qc2ApOyAvLyBlc2xpbnQtZGlzYWJsZS1saW5lIGdsb2JhbC1yZXF1aXJlLGltcG9ydC9uby1keW5hbWljLXJlcXVpcmVcbiAgc2V0dGluZ3MgPSBzZXR0aW5ncy5kZWZhdWx0O1xuXG4gIGxldCBhZHMgPSBbXTtcbiAgY29uc3QgcGFyc2VyID0gbmV3IFBhcnNlcigpO1xuXG4gIGZvciAoY29uc3QgcGFyc2VyU2V0dGluZ3Mgb2Ygc2V0dGluZ3MucGFyc2Vycykge1xuICAgIC8qIGVzbGludC1kaXNhYmxlIG5vLWF3YWl0LWluLWxvb3AgKi9cbiAgICBjb25zdCBwYXJzZXJDbGFzcyA9IHJlcXVpcmUoYC4vcGFyc2Vycy8ke3BhcnNlclNldHRpbmdzLnBhcnNlcn0uanNgKTsgLy8gZXNsaW50LWRpc2FibGUtbGluZSBnbG9iYWwtcmVxdWlyZSxpbXBvcnQvbm8tZHluYW1pYy1yZXF1aXJlXG5cbiAgICBwYXJzZXIuc2V0VXJsKHBhcnNlclNldHRpbmdzLnVybCk7XG4gICAgcGFyc2VyLnNldFNlYXJjaElkKHNldHRpbmdzRmlsZU5hbWUpO1xuICAgIHBhcnNlci5zZXRQYXJzZXIocGFyc2VyQ2xhc3MuZGVmYXVsdCk7XG4gICAgY29uc3QgcGFyc2VkQWRzID0gYXdhaXQgcGFyc2VyLnBhcnNlKCkuY2F0Y2goZXJyID0+IGNvbnNvbGUubG9nKGVycikpO1xuICAgIGFkcyA9IGFkcy5jb25jYXQocGFyc2VkQWRzKTtcbiAgfVxuXG4gIGNvbnN0IG5vdGlmaWVyID0gbmV3IE5vdGlmaWVyKHNldHRpbmdzLmlmdHR0LmV2ZW50LCBzZXR0aW5ncy5pZnR0dC5rZXkpO1xuXG4gIC8qIGVzbGludCAnbm8tcmVzdHJpY3RlZC1zeW50YXgnOiBbJ2Vycm9yJywgJ0xhYmVsZWRTdGF0ZW1lbnQnLCAnV2l0aFN0YXRlbWVudCddICovXG4gIGZvciAoY29uc3QgYWQgb2YgYWRzKSB7XG4gICAgLyogZXNsaW50LWRpc2FibGUgbm8tYXdhaXQtaW4tbG9vcCAqL1xuICAgIGF3YWl0IG5vdGlmaWVyLm5vdGlmeShhZC50aXRsZSwgYWQubGluaywgYWQuaW1hZ2UpLmNhdGNoKGVyciA9PiBjb25zb2xlLmxvZyhlcnIpKTtcbiAgfVxufVxuXG5Dcm9uLnNjaGVkdWxlKCcqICogKiAqIConLCBhc3luYyAoKSA9PiB7XG4gIGNvbnNvbGUuaW5mbyhgWyR7c2V0dGluZ3NGaWxlTmFtZX1dIFBhcnNpbmcgc3RhcnRlZCAuLi5gKTtcbiAgYXdhaXQgaW5pdCgpO1xuICBjb25zb2xlLmluZm8oYFske3NldHRpbmdzRmlsZU5hbWV9XSBQYXJzaW5nIGVuZGVkICFgKTtcbn0pO1xuIl19
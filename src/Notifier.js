import request from 'request-promise-native';

export default class Notifier {
  constructor(eventName, key) {
    this.eventName = eventName || null;
    this.key = key || null;
  }

  set setEventName(eventName) {
    if (eventName) {
      this.eventName = eventName;
    }
  }

  set setKey(key) {
    if (key) {
      this.key = key;
    }
  }

  async notify(value1, value2, value3) {
    return request.post(`https://maker.ifttt.com/trigger/${this.eventName}/with/key/${this.key}`, {
      body: {
        value1,
        value2,
        value3,
      },
      json: true,
    }, (err, httpResponse, body) => {
      if (err) {
        throw Error(`Error: ${err}\nStatusCode: ${httpResponse && httpResponse.statusCode}\nBody: ${body}`);
      }
    });
  }
}

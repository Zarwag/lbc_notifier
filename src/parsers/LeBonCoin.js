import { JSDOM } from 'jsdom';

export default class LeBonCoin {
  constructor(url) {
    this.url = url;
    this.baseUrl = 'https://leboncoin.fr/';
  }

  async parse() {
    const dom = await JSDOM.fromURL(this.url, {
      referrer: this.baseUrl,
      runScripts: 'dangerously',
    }).catch(err => console.log(err));

    const data = dom.window.FLUX_STATE.adSearch.data.ads;

    const ads = [];

    Array.prototype.forEach.call(data, (ad) => {
      // const adDate = new Date(ad.index_date);

      ads.push({
        id: ad.list_id,
        title: `LBC - ${ad.subject}`,
        // date: `${adDate.getHours()}:${adDate.getMinutes()}`,
        // location: ad.location.city_label,
        // price: ad.price[0],
        // category: ad.category_name,
        image: ad.images.nb_images > 0 ? ad.images.thumb_url : null,
        link: ad.url,
      });
    });

    return ads;
  }
}

import { JSDOM } from 'jsdom';

export default class OuestFranceImmo {
  constructor(url) {
    this.url = url;
    this.baseUrl = 'https://www.ouestfrance-immo.com';
  }

  async parse() {
    const dom = await JSDOM.fromURL(this.url, {
      referrer: this.baseUrl,
    }).catch(err => console.log(err));

    const adsList = dom.window.document.querySelectorAll('.annLink');

    const ads = [];

    const { baseUrl } = this;

    Array.prototype.forEach.call(adsList, (ad) => {
      if (!ad.querySelector('div:first-child').classList.contains('premium')) {
        // const adDate = ad.querySelector('.annDebAff')
        //   ? new Date(ad.querySelector('.annDebAff').textContent.trim())
        //   : new Date();
        const adAdresse = ad.querySelector('.annAdresse') ? ad.querySelector('.annAdresse').textContent.trim() : '';

        ads.push({
          id: parseInt(ad.querySelector('div:first-child').getAttribute('data-id'), 10),
          title: `OFI - ${ad.querySelector('.annTitre').textContent.trim()} ${adAdresse}`,
          // date: `${adDate.getHours()}:${adDate.getMinutes()}`,
          // location: adAdresse,
          // price: ad.querySelector('.annPrix').textContent.trim(),
          // category: null,
          image: ad.querySelector('.annPhoto').getAttribute('data-original'),
          link: baseUrl + ad.getAttribute('href'),
        });
      }
    });

    return ads;
  }
}

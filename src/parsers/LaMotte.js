import { JSDOM } from 'jsdom';

export default class LaMotte {
  constructor(url) {
    this.url = url;
    this.baseUrl = 'https://www.lamotte-gestion.fr';
  }

  async parse() {
    const dom = await JSDOM.fromURL(this.url, {
      referrer: this.baseUrl,
    }).catch(err => console.log(err));

    const adsList = dom.window.document.querySelectorAll('.programme');

    const ads = [];

    Array.prototype.forEach.call(adsList, (ad) => {
      const link = ad.querySelector('a:first-child').getAttribute('href');
      const id = /(\d+)$/g.exec(link);

      ads.push({
        id: parseInt(id[1], 10),
        title: `LM - ${ad.querySelector('.nom-programme').textContent.trim()} ${ad.querySelector('.reference').textContent.trim()}`,
        image: ad.querySelector('.programme-img img').getAttribute('src'),
        link: this.baseUrl + link,
      });
    });

    return ads;
  }
}

import { JSDOM } from 'jsdom';

export default class ThierryImmobilier {
  constructor(url) {
    this.url = url;
    this.baseUrl = 'https://www.thierry-immobilier.fr';
  }

  async parse() {
    const dom = await JSDOM.fromURL(this.url, {
      referrer: this.baseUrl,
    }).catch(err => console.log(err));

    const adsList = dom.window.document.querySelectorAll('.teaser--immobilier');

    const ads = [];

    const { baseUrl } = this;

    Array.prototype.forEach.call(adsList, (ad) => {
      const link = ad.querySelector('a:first-child').getAttribute('href');
      const id = /(\d+)\?/g.exec(link);

      ads.push({
        id: parseInt(id[1], 10),
        title: `TI - ${ad.querySelector('.teaser__title').textContent.trim()}`,
        image: ad.querySelector('.field-name-immobilier-photos img').getAttribute('src'),
        link: baseUrl + link,
      });
    });

    return ads;
  }
}

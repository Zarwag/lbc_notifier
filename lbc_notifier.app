[Unit]
Description=LeBonCoin Notifier

[Service]
ExecStart=/home/jeram/lbc_notifier/dist/cli.js
Restart=always
User=nogroup
# Note RHEL/Fedora uses 'nobody', Debian/Ubuntu uses 'nogroup'
Group=nogroup
Environment=PATH=/usr/bin:/usr/local/bin
Environment=NODE_ENV=production
WorkingDirectory=/home/jeram/lbc_notifier

[Install]
WantedBy=multi-user.target
